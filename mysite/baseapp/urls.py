from django.urls import path
from django.conf.urls import url

from . import views

app_name = 'baseapp'
urlpatterns = [
    path('', views.index, name='index'),
    # path('', views.signup, name='index'),
    # path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    # path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    # path('<int:question_id>/vote/', views.vote, name='vote'),
    url(r'signup/$', views.signup, name='signup')    # path('signup/', views.signup, name='signup')
]