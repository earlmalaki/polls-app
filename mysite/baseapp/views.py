from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect

from django.views import generic

from django.http import HttpResponse

# Create your views here.
def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/polls/')
    else:
        form = UserCreationForm()
    return render(request, 'baseapp/signup.html', {'form': form})


def index(request):
  return render(request, 'baseapp/base.html')

